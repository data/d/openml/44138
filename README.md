# OpenML dataset: houses

https://www.openml.org/d/44138

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "regression on numerical features" benchmark. Original description: 
 
**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

S&P Letters Data
We collected information on the variables using all the block groups in California from the 1990 Census. In this sample a block group on average includes 1425.5 individuals living in a geographically compact area. Naturally, the geographical area included varies inversely with the population density. We computed distances among the centroids of each block group as measured in latitude and longitude. We excluded all the block groups reporting zero entries for the independent and dependent variables. The final data contained 20,640 observations on 9 variables. The dependent variable is ln(median house value).

Bols	tols
INTERCEPT		11.4939	275.7518
MEDIAN INCOME		0.4790	45.7768
MEDIAN INCOME2		-0.0166	-9.4841
MEDIAN INCOME3		-0.0002	-1.9157
ln(MEDIAN AGE)		0.1570	33.6123
ln(TOTAL ROOMS/ POPULATION)	-0.8582	-56.1280
ln(BEDROOMS/ POPULATION)	0.8043	38.0685
ln(POPULATION/ HOUSEHOLDS)	-0.4077	-20.8762
ln(HOUSEHOLDS)		0.0477	13.0792

The file cadata.txt contains all th

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44138) of an [OpenML dataset](https://www.openml.org/d/44138). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44138/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44138/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44138/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

